SAL_DIR = /tmp/spiffy-form-data-salmonella/


default: module


module: spiffy-form-data.scm
	csc -s -O3 -j spiffy-form-data spiffy-form-data.scm


test: module tests/run.scm
	chicken-install -test -sudo

salmonella:
	rm -rf $(SAL_DIR)
	mkdir -p $(SAL_DIR)
	salmonella --log-file=$(SAL_DIR)log
	salmonella-html-report $(SAL_DIR)log $(SAL_DIR)html
	firefox $(SAL_DIR)html/index.html

clean:
	rm -f *.o *.so spiffy-form-data.import.scm
	rm -f test/spiffy-form-data.so test/spiffy-form-data.import.scm test/run

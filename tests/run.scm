(use test spiffy-form-data intarweb spiffy multipart-form-data)



;;; ===========================================================================
;;; Helper functions
;;; ===========================================================================
(define (lf->crlf str)
  (string-translate* str '(("\n" . "\r\n"))))
(define wifs with-input-from-string)
(define cin current-input-port)

;;; Set the (current-request) parameter to an intarweb request object by
;;; parsing 'str' as an HTTP request.
(define (with-current-request str thunk)
  (wifs str (lambda ()
    (parameterize ((current-request (read-request (cin))))
      (thunk)))))





;;; ===========================================================================
;;; Test data
;;; ===========================================================================
(define ex (lf->crlf #<<END
POST http://localhost:50460/api/values/1 HTTP/1.1
User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: en-us,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: multipart/form-data; boundary=---------------------------41184676334
Content-Length: 29278

-----------------------------41184676334
Content-Disposition: form-data; name="caption"

Summer vacation
-----------------------------41184676334
Content-Disposition: form-data; name="image1"; filename="GrandCanyon.jpg"
Content-Type: image/jpeg

binary data here...

END
))




(test-group "spiffy-form-data"

  (with-current-request ex (lambda ()
    (with-form-data (lambda (get)
      (test "field value"
            "Summer vacation"
            (read-string #f (form-datum-port (get 'caption))))
      (test "non-existant field"
            #f
            (get 'foobar))
      (test "test-file-backing"
            #f
            (form-datum-tempfile (get 'caption)))
      (test "filename"
            "GrandCanyon.jpg"
            (form-datum-filename (get 'image1)))
      (test "content-type"
            "image/jpeg"
            (form-datum-ctype (get 'image1))))))))



(test-exit)

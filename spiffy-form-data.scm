;;; Access to multipart/form-data fields in spiffy.
;;;
;;;
;;;  
;;; Copyright (C) 2013, Bryan Vicknair
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are met:
;;; 
;;; Redistributions of source code must retain the above copyright notice, this
;;; list of conditions and the following disclaimer.  Redistributions in binary
;;; form must reproduce the above copyright notice, this list of conditions and
;;; the following disclaimer in the documentation and/or other materials
;;; provided with the distribution.  Neither the name of the author nor the
;;; names of its contributors may be used to endorse or promote products
;;; derived from this software without specific prior written permission. 
;;; 
;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;;; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
;;; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;;; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;;; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
;;; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;;; POSSIBILITY OF SUCH DAMAGE.

 
; TODO: Need convienient macro that binds variables to the form-datum in
; an alist in side of a context.  Like spiffy-request-var's with-request-vars.
; This needs a way to say "for this field, just load the data into mem and give
; me the value, and for others, give me a form-datum."  For regular fields, we
; don't want a form datum.
 
; TODO: Maybe a form-data egg that deals with getting all HTML form data.  Can
; use multipart-form-data and uri-common/etc to parse urlencoded GET and POST.
; form-data egg can mostly be framework agnostic, requiring
; content-type/boundary/url etc as input.  Then it will be easy to write small
; eggs that adapt it to a particular framework like intarweb/spiffy/awful.
; Splitting the parsing of form field values into spiffy-form-data and
; spiffy-request-vars is ugly.  Also, I think a form-data egg should simply
; provide access to the form values as submitted.  Converting those values to
; expected "types" etc is the job of another "form-schema" egg.

(module spiffy-form-data
  (with-form-data)


(import scheme chicken)
(use intarweb spiffy multipart multipart-form-data srfi-1)



;;; thunk is called w/ a function that when given a field name returns a
;;; form-datum record from the multipart-form-data egg, or #f if the field
;;; isn't in the form.
(define (with-form-data thunk)
  (let* ((alist (form-data-alist (current-request)))
         (cleanup-pair (lambda (p) (cleanup-form-datum (cdr p))))
         (cleanup (lambda () (for-each cleanup-pair alist))))
    (handle-exceptions exn (begin (cleanup)
                                  (abort exn))
      (thunk (form-data-alist-lookup alist)))
    (cleanup)))




; alist of (field-name . form-datum), where form-datum is a record from the
; multipart-form-data egg.  request should be an intarweb request record.
(define (form-data-alist request)
  (let* ((hdrs (request-headers request))
         (ctype (header-value 'content-type hdrs)))
    (if (not (eq? ctype 'multipart/form-data))
      #f
      (let* ((boundary (header-param 'boundary 'content-type hdrs))
             (clen (header-value 'content-length hdrs))
             (port (request-port request))
             (mkpair (lambda (mime-part-port)
                       (let ((field (mime-part->form-datum mime-part-port)))
                         (cons (form-datum-name field) field)))))

        (map-mime-parts mkpair
                        (make-len-delimited-input-port port clen)
                        boundary)))))


;;; Allows getting field by name which can be a symbol or string.
(define (form-data-alist-lookup alist)
  (lambda (field)
    (let* ((field (if (symbol? field) (symbol->string field) field))
           (found (assoc field alist)))
      (if found (cdr found) #f))))


;;; Delete form-datum backing tempfile if it exists.
(define (cleanup-form-datum field)
  (let ((tempfile (form-datum-tempfile field)))
    (when (and tempfile (file-exists? tempfile))
      (delete-file (form-datum-tempfile field)))))



)  ; end module

